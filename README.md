# README #  
  
This README would normally document whatever steps are necessary to get your application up and running.  
  
### How to launch? ###  
  
1) go to root project directory and run ```docker-compose build```  
2) After building install dependancies inside docker containers:  
```docker-compose run node bash -c "cd frontend && npm install --no-bin-links"``` (--no-bin-links option requires to windows system)  
```docker-compose run php bash -c "cd sf4 && composer install" ```  
3) Then run ```docker-compose up``` ant app should run  
Symfony: http://localhost:82  
Angular: http://localhost:4200  
  
PS. If for some reasons angular project not accessing symfony by localhost, change it to your docker ip in ```map.component.ts```. This installation guide is for Windows Docker Toolbox.  

Also you probably need change docker-compose file version depending on your OS or docker. Docker Toolbox maintain only 2 version.