<?php

namespace App\Tests\Service;

use App\Service\OpenWeatherProvider;
use PHPUnit\Framework\TestCase;

class OpenWeatherProviderTest extends TestCase
{
    public function testData()
    {
        $provider = new OpenWeatherProvider('', '', '');
        $provider->loadData('', '');
        $data = $provider->getData();
        $this->assertEquals([
            'temp' => 'N/A',
            'provider' => 'OpenWeatherProvider',
	    ], $data);
    }
}