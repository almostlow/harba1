<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\DelegatingWeatherProvider;

class HomeController extends AbstractController
{
    /**
     * @Route("/get-data")
    */
    public function getData()
    {
        try {
            $url = $this->getParameter('data_url');
            $data = file_get_contents($url);
            $return = ['success' => true, 'data' => json_decode($data)];
            $response = new JsonResponse($return, 200, ['Access-Control-Allow-Origin' => '*']);
            return $response;
        } catch (\Exception $e) {
            $response = new JsonResponse(['success' => false, 'reason' => 'No available data currently'], 200, ['Access-Control-Allow-Origin' => '*']);
            return $response;
        } 
        
    }

    /**
     * @Route("/get-weather/{lon}/{lat}")
    */
    public function getWeather(string $lon, string $lat, DelegatingWeatherProvider $provider)
    {
        $data = $provider->getWeatherByLatLon($lon, $lat);
        $response = new JsonResponse($data, 200, ['Access-Control-Allow-Origin' => '*']);
        return $response;
    }
}
