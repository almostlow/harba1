<?php

namespace App\Service;

use App\Service\WeatherProviderInterface;

class OpenWeatherProvider implements WeatherProviderInterface
{
    private $apiKey;
    private $apiUrl;
    private $unit;
    private $provider_name;
    private $status;
    private $data;

    public function __construct(string $apiKey, string $apiUrl, string $unit)
    {
        $this->apiKey = $apiKey;
        $this->apiUrl = $apiUrl;
        $this->unit = $unit;
        $this->provider_name = 'OpenWeatherProvider';
        $this->status = 'Unknown';
        $this->data = [];
    }

    /**
     * Loading data from yahoo weather provider by given longitude and latitude
     * @param string $lon - coordinates part
     * @param string $lat - coordinates part
     */
    public function loadData(string $lon, string $lat)
    {
        try {
            $url = $this->apiUrl . '?lat=' . rawurlencode($lat) . '&lon=' . rawurlencode($lon) . '&units=' . $this->unit . '&appid=' . $this->apiKey;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            $result = curl_exec($ch);
            curl_close($ch);
            $decoded = json_decode($result);
            if (isset($decoded->main->temp)) {
                $temp = $decoded->main->temp;
                $this->status = 'success';
            } else {
                $temp = 'N/A';
                $this->status = 'error';
            }
            $this->data = [
                'temp' => $temp,
                'provider' => $this->provider_name,
            ];
        } catch (\Exception $e) {
            $this->data = [
                'temp' => 'N/A',
                'provider' => $this->provider_name,
            ];
        }
    }

    /**
     * Getting data after data loading in loadData method
     * @param string $lon - coordinates part
     * @return array - ['temp' => '22', 'provider' => 'OpenWeatherProvider'] - temperature and weather provider
     */
    public function getData():array
    {
        return $this->data;
    }

    public function isSuccessfull()
    {
        return $this->status === 'success';
    }
}
