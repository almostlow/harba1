<?php

namespace App\Service;

interface WeatherProviderInterface
{
	/**
     * Loading data from weather provider by given longitude and latitude
     * @param string $lon - coordinates part
     * @param string $lat - coordinates part
     */
    public function loadData(string $lon, string $lat);

    /**
     * Getting data after data loading in loadData method
     * @param string $lon - coordinates part
     * @return array - ['temp' => '22', 'provider' => 'OpenWeatherProvider'] - temperature and weather provider
     */
    public function getData():array;

    /**
     * Checking if response from api was successfull
     * @return boolean
     */
    public function isSuccessfull();
}
