<?php

namespace App\Service;

use App\Service\WeatherProviderInterface;

class YahooWeatherProvider implements WeatherProviderInterface
{
    private $appId;
    private $apiUrl;
    private $unit;
    private $provider_name;
    private $status;
    private $data;
    private $consumerKey;
    private $consumerSecret;

    public function __construct(string $appId, string $apiUrl, string $unit, string $consumerKey, string $consumerSecret)
    {
        $this->appId = $appId;
        $this->apiUrl = $apiUrl;
        $this->unit = $unit;
        $this->provider_name = 'YahooWeatherProvider';
        $this->status = 'Unknown';
        $this->data = [];
        $this->consumerKey = $consumerKey;
        $this->consumerSecret = $consumerSecret;
    }

    /**
     * Build api url
     * 
     * @param string $baseURI - api url
     * @param string $method - http method
     * @param array $params - url parameters
     * @return string - builded url
     */
    private function buildBaseString(string $baseURI, string $method, array $params)
    {
        $r = array();
        ksort($params);
        foreach ($params as $key => $value) {
            $r[] = "$key=" . rawurlencode($value);
        }
        return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
    }

    /**
     * Build authorization header
     * 
     * @param string $baseURI - api url
     * @param string $method - http method
     * @param array $params - url parameters
     * @return string - builded authorization header
     */
    private function buildAuthorizationHeader($oauth)
    {
        $r = 'Authorization: OAuth ';
        $values = array();
        foreach ($oauth as $key=>$value) {
            $values[] = "$key=\"" . rawurlencode($value) . "\"";
        }
        $r .= implode(', ', $values);
        return $r;
    }

    /**
     * Defines oauth parameters
     * 
     * @return array - oauth parameters
     */

    private function oauth()
    {
        return [
            'oauth_consumer_key' => $this->consumerKey,
            'oauth_nonce' => uniqid(mt_rand(1, 1000)),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        ];
    }
    
    /**
     * Loading data from yahoo weather provider by given longitude and latitude
     * @param string $lon - coordinates part
     * @param string $lat - coordinates part
     */
    public function loadData(string $lon, string $lat)
    {
        try {
            $oauth = $this->oauth();
            $query = array(
                'lat' => $lat,
                'lon' => $lon,
                'format' => 'json',
                'u' => $this->unit,
            );
            $baseInfo = $this->buildBaseString($this->apiUrl, 'GET', array_merge($query, $oauth));
            $compositeKey = rawurlencode($this->consumerSecret) . '&';
            $oauthSignature = base64_encode(hash_hmac('sha1', $baseInfo, $compositeKey, true));
            $oauth['oauth_signature'] = $oauthSignature;
            $header = array(
                $this->buildAuthorizationHeader($oauth),
                'X-Yahoo-App-Id: ' . $this->appId,
            );
            $options = array(
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_HEADER => false,
                CURLOPT_URL => $this->apiUrl . '?' . http_build_query($query),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false
            );
            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            $decoded = json_decode($response);
            if (isset($decoded->current_observation->condition->temperature)) {
                $temp = $decoded->current_observation->condition->temperature;
                $this->status = 'success';
            } else {
                $temp = 'N/A';
                $this->status = 'error';
            }
            $this->data = [
                'temp' => $temp,
                'provider' => $this->provider_name,
            ];
        } catch (\Exception $e) {
            $this->data = [
                'temp' => 'N/A',
                'provider' => $this->provider_name,
            ];
        }
    }

    /**
     * Getting data after data loading in loadData method
     * @param string $lon - coordinates part
     * @return array - ['temp' => '22', 'provider' => 'YahooWeatherProvider'] - temperature and weather provider
     */
    public function getData():array
    {
        return $this->data;
    }

    /**
     * Checking if response from api was successfull
     * @return boolean
     */
    public function isSuccessfull()
    {
        return $this->status === 'success';
    }
}
