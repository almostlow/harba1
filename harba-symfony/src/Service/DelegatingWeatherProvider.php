<?php

namespace App\Service;

use App\Service\OpenWeatherProvider;
use App\Service\YahooWeatherProvider;

class DelegatingWeatherProvider
{
    private $weatherProviders;
    private $yahooWeatherProvider;

    public function __construct(OpenWeatherProvider $openWeatherProvider, YahooWeatherProvider $yahooWeatherProvider)
    {
        $this->weatherProviders = [$openWeatherProvider, $yahooWeatherProvider];
    }

    /**
     * Getting data from one of weather providers. If one is not available, going to second one.
     * If no one available, returning without data ['temp' => 'N/A', 'provider' => 'No data available']
     * @param string $lon - coordinates part
     * @param string $lat - coordinates part
     * @return array - ['temp' => '22', 'provider' => 'OpenWeatherProvider'] - temperature and weather provider
     */
    public function getWeatherByLatLon(string $lon, string $lat):array
    {
        foreach ($this->weatherProviders as $provider) {
            $provider->loadData($lon, $lat);
            if ($provider->isSuccessfull()) {
                return $provider->getData();
            }
        }

        return ['temp' => 'N/A', 'provider' => 'No data available'];
    }
}
