import { AfterViewInit, Component, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as L from 'leaflet';

const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = L.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});
L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./map.component.css', '../../../node_modules/leaflet/dist/leaflet.css']
})
export class MapComponent implements AfterViewInit {
  private map;
  private service_url;
  private harbos_data;
  private provider_url;
  private weather_provider_url;
  private usage_http;
  private data;

  constructor(private http: HttpClient) { }

  ngAfterViewInit(): void {
    this.initMap();
    const usage_http = this.http;
    const provider = this.provider_url;
    const weather_provider = this.weather_provider_url;
    usage_http.get(this.service_url)
      .subscribe(data => {
        if (data['success']) {
          Object.entries(data['data']).forEach(([key, val]) => {
            const marker = L.marker([val['lon'], val['lat']]).addTo(this.map);
            this.data.name = val['name'];
            this.data.image = val['image'];
            marker.bindPopup(``);
          });
        }
      });
    const data = this.data;
    this.map.on('popupopen', function(e) {
      const latlng = e.popup.getLatLng();
      e.popup.setContent(`Loading data. Be patient..`);
      usage_http.get(`${weather_provider}/${latlng.lng}/${latlng.lat}`)
        .subscribe(return_data => {
          e.popup.setContent(`<strong>Name</strong>: ${data['name']}<br><img style="max-width: 100%;" src=${provider}${data['image']} />`)
          const content = e.popup.getContent();
          e.popup.setContent(content + `<br><strong>Temp: </strong>${return_data['temp']} °C<br><strong>Provider: </strong>${return_data['provider']}` );
        });
    });
  }

  private initMap(): void {
    this.map = L.map('map', {
      center: [ 39.8282, -98.5795 ],
      zoom: 3
    });
    this.service_url = 'http://localhost:82/get-data';
    this.weather_provider_url = 'http://localhost:82/get-weather';
    this.provider_url = 'https://devapi.harba.co';
    this.data = {};
    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
  }
}